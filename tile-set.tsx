<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.9" tiledversion="1.9.2" name="tiles-simple" tilewidth="32" tileheight="32" tilecount="135" columns="15">
 <image source="tiles.png" width="480" height="288"/>
 <wangsets>
  <wangset name="walls" type="mixed" tile="-1">
   <wangcolor name="thick-wall" color="#ff0000" tile="-1" probability="1"/>
   <wangcolor name="space" color="#00ff00" tile="-1" probability="1"/>
   <wangcolor name="thin-wall" color="#0000ff" tile="-1" probability="1"/>
   <wangcolor name="cave" color="#ff7700" tile="-1" probability="1"/>
   <wangtile tileid="3" wangid="1,1,1,2,1,1,1,1"/>
   <wangtile tileid="4" wangid="1,1,1,2,2,2,1,1"/>
   <wangtile tileid="5" wangid="1,1,1,1,1,2,1,1"/>
   <wangtile tileid="6" wangid="2,2,2,3,2,2,2,2"/>
   <wangtile tileid="7" wangid="2,2,2,3,3,3,2,2"/>
   <wangtile tileid="8" wangid="2,2,2,2,2,3,2,2"/>
   <wangtile tileid="9" wangid="4,4,4,2,4,4,4,4"/>
   <wangtile tileid="10" wangid="4,4,4,2,2,2,4,4"/>
   <wangtile tileid="11" wangid="4,4,4,4,4,2,4,4"/>
   <wangtile tileid="12" wangid="2,2,2,4,2,2,2,2"/>
   <wangtile tileid="13" wangid="2,2,2,4,4,4,2,2"/>
   <wangtile tileid="14" wangid="2,2,2,2,2,4,2,2"/>
   <wangtile tileid="18" wangid="1,2,2,2,1,1,1,1"/>
   <wangtile tileid="19" wangid="2,2,2,2,2,2,2,2"/>
   <wangtile tileid="20" wangid="1,1,1,1,1,2,2,2"/>
   <wangtile tileid="21" wangid="2,3,3,3,2,2,2,2"/>
   <wangtile tileid="22" wangid="3,3,3,3,3,3,3,3"/>
   <wangtile tileid="23" wangid="2,2,2,2,2,3,3,3"/>
   <wangtile tileid="24" wangid="4,2,2,2,4,4,4,4"/>
   <wangtile tileid="26" wangid="4,4,4,4,4,2,2,2"/>
   <wangtile tileid="27" wangid="2,4,4,4,2,2,2,2"/>
   <wangtile tileid="29" wangid="2,2,2,2,2,4,4,4"/>
   <wangtile tileid="33" wangid="1,2,1,1,1,1,1,1"/>
   <wangtile tileid="34" wangid="2,2,1,1,1,1,1,2"/>
   <wangtile tileid="35" wangid="1,1,1,1,1,1,1,2"/>
   <wangtile tileid="36" wangid="2,3,2,2,2,2,2,2"/>
   <wangtile tileid="37" wangid="3,3,2,2,2,2,2,3"/>
   <wangtile tileid="38" wangid="2,2,2,2,2,2,2,3"/>
   <wangtile tileid="39" wangid="4,2,4,4,4,4,4,4"/>
   <wangtile tileid="40" wangid="2,2,4,4,4,4,4,2"/>
   <wangtile tileid="41" wangid="4,4,4,4,4,4,4,2"/>
   <wangtile tileid="42" wangid="2,4,2,2,2,2,2,2"/>
   <wangtile tileid="43" wangid="4,4,2,2,2,2,2,4"/>
   <wangtile tileid="44" wangid="2,2,2,2,2,2,2,4"/>
   <wangtile tileid="48" wangid="1,2,2,2,2,2,1,1"/>
   <wangtile tileid="49" wangid="1,1,1,2,2,2,2,2"/>
   <wangtile tileid="50" wangid="2,2,2,2,1,1,1,2"/>
   <wangtile tileid="51" wangid="2,3,3,3,3,3,2,2"/>
   <wangtile tileid="52" wangid="2,2,2,3,3,3,3,3"/>
   <wangtile tileid="53" wangid="3,3,3,3,2,2,2,3"/>
   <wangtile tileid="54" wangid="4,2,2,2,2,2,4,4"/>
   <wangtile tileid="55" wangid="4,4,4,2,2,2,2,2"/>
   <wangtile tileid="56" wangid="2,2,2,2,4,4,4,2"/>
   <wangtile tileid="57" wangid="4,2,2,4,4,4,4,4"/>
   <wangtile tileid="58" wangid="4,4,4,4,4,4,2,2"/>
   <wangtile tileid="59" wangid="4,4,2,2,4,4,4,4"/>
   <wangtile tileid="63" wangid="2,2,1,1,1,2,2,2"/>
   <wangtile tileid="64" wangid="1,1,1,1,1,1,1,1"/>
   <wangtile tileid="66" wangid="3,3,2,2,2,3,3,3"/>
   <wangtile tileid="69" wangid="2,2,4,4,4,2,2,2"/>
   <wangtile tileid="70" wangid="4,4,4,4,4,4,4,4"/>
   <wangtile tileid="71" wangid="4,4,2,2,2,4,4,4"/>
   <wangtile tileid="72" wangid="4,4,4,4,4,2,2,4"/>
   <wangtile tileid="73" wangid="4,4,4,2,2,4,4,4"/>
   <wangtile tileid="74" wangid="4,4,4,4,2,2,4,4"/>
   <wangtile tileid="84" wangid="2,4,4,4,4,4,2,2"/>
   <wangtile tileid="85" wangid="2,2,2,4,4,4,4,4"/>
   <wangtile tileid="86" wangid="4,4,4,4,2,2,2,4"/>
   <wangtile tileid="87" wangid="2,2,4,4,4,4,4,4"/>
   <wangtile tileid="88" wangid="2,4,4,4,4,4,4,2"/>
   <wangtile tileid="102" wangid="2,4,4,2,2,2,2,2"/>
   <wangtile tileid="103" wangid="2,2,2,2,2,2,4,4"/>
   <wangtile tileid="104" wangid="2,2,4,4,2,2,2,2"/>
   <wangtile tileid="117" wangid="2,2,2,2,2,4,4,2"/>
   <wangtile tileid="118" wangid="2,2,2,4,4,2,2,2"/>
   <wangtile tileid="119" wangid="2,2,2,2,4,4,2,2"/>
   <wangtile tileid="132" wangid="4,4,2,2,2,2,2,2"/>
   <wangtile tileid="133" wangid="4,2,2,2,2,2,2,4"/>
  </wangset>
 </wangsets>
</tileset>
