# Simple tiles for TTRPG

Simple tiles for TTRPG map creation. [Tiled](https://www.mapeditor.org/) tile set prepared for terrain tool.

![Thick walls](examples/example.png)
![Thin walls](examples/thin-wall-example.png)
![Thick walls](examples/cave-example.png)

## Usage

- fill map with base tile
- use terrain tool

## Roadmap

- improved visuals
- step by step guide

## Contributing

I will accept reasonable PRs.

## License

GNU
